優子ちゃんのFlash
====

## 説明

優子ちゃんのFlashをオープンソース化したものです。

## ファイル説明

### ActionScriptファイル

   * _global.as 

### 

エプロン
---

   * yuko01.fla
   * yuko01.cmox
   * yuko01.canx
   * yuko01.swf

スーツ
---

   * yuko02.fla
   * yuko02.canx
   * yuko02.swf
   * yuko02.cmox

セーラー服
---

   * yuko03.fla
   * yuko03.canx
   * yuko03.swf
   * yuko03.cmox

画像ファイル
---

`image` フォルダ内

足画像
===

   * foot1.png

エプロン画像・上
===

   * image.png

エプロン画像・下
===

   * image1.png

セーラー服のPhotoShopデータ　
===

   * se-ra-.psd

スーツ画像
===

   * suit.png

セーラー服
===

   * uniform.png
 

## 使用方法

### 口パクのさせ方。

FlashのActionScript内にメソッドがあります。
HTMLでは以下のようにエプロン姿の優子ちゃんを使用していると想定。
`embed`タグに対する`id`と`name`の名前をつけておいて下さい。
名前は、`as3`としています。
```
<object id="flash-obj" type="application/x-shockwave-flash"
    width="190%" height="100%"
    data=""path/to/yuko01.swf"">
    <param name="wmode" value="transparent" />
    <param name="movie" value="path/to/yuko01.swf" />
    <param name="quality" value="high" />
    <!-- 消してはいけない。nameのas3をJSから呼ぶのに必要。 -->
    <embed id="as3" name="as3" src="path/to/yuko01.swf"
        allowScriptAccess="always" width="350" height="720"
        wmode="transparent">
    </embed>
</object>
```

JavaScriptで以下のように`flash`変数を宣言しておき、
```
var flash = document.as3 || window.as3;
```

以下の5種類のメソッド
```
flash.as_function1();
flash.as_function2();
flash.as_function3();
flash.as_function4();
flash.as_function5();
```
で呼べます。

### 口パクの止め方。

```
flash.someMethod(duration);
```
で呼べます。
`duration`は止める時間を入力します。

## Contribution

Shannon Lab株式会社

## ライセンス

未定

## Author

[Shannon Lab](https://bitbucket.org/shannondev)
jmindview@gmail.comまでご連絡下さい。
